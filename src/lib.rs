use camino::Utf8Path as Path;
use camino::Utf8PathBuf as PathBuf;
use color_eyre::eyre::Context;
use color_eyre::Report;
use color_eyre::Result;
use nom::combinator::all_consuming;
use nom::Finish;

use std::fmt::Display;
use std::fs::read_to_string;

use nom::{
    character::complete::{char, multispace1, newline, space1},
    combinator::{map, opt, rest},
    multi::separated_list0,
    sequence::{delimited, pair, tuple},
    IResult,
};

fn parse_author_line(string: &str) -> IResult<&str, Name> {
    let parse_author = rest;
    let before = tuple((multispace1, char('―'), space1));
    map(delimited(before, parse_author, newline), |s: &str| {
        Name(s.to_string())
    })(string)
}

fn parse_fortune(string: &str) -> IResult<&str, Fortune> {
    let parse_quote = map(rest, |s: &str| Quote(s.to_string()));
    let (remaining, (quote, maybe_author)) = pair(parse_quote, opt(parse_author_line))(string)?;
    match maybe_author {
        Some(author) => Ok((remaining, Fortune::WithAuthor(quote, author))),
        None => Ok((remaining, Fortune::Anonymous(quote))),
    }
}

fn parse_fortune_list(string: &str) -> IResult<&str, Vec<Fortune>> {
    separated_list0(char('%'), parse_fortune)(string)
}

pub enum Fortune {
    Anonymous(Quote),
    WithAuthor(Quote, Name),
}

#[derive(Debug)]
pub struct Quote(String);
impl Display for Quote {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug)]
pub struct Name(String);
impl Display for Name {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

// fn parse_fortunes<'a>(string: &'a str) -> Result<Fortunes> {
//     all_consuming(parse_fortune_list)(string)
//         .finish()
//         .map(|(_, fortunes)| Fortunes(fortunes))
//         .wrap_err("Not a valid list of fortunes")
// }

pub struct Fortunes(Vec<Fortune>);
// impl TryFrom<&str> for Fortunes {
//     type Error = Report;

//     fn try_from(string: &str) -> std::result::Result<Self, Self::Error> {
//         all_consuming(parse_fortune_list)(string)
//             .finish()
//             .map(|(_, fortunes)| Fortunes(fortunes))
//             .wrap_err("Not a valid list of fortunes")
//     }
// }
// impl TryFrom<String> for Fortunes {
//     type Error = Report;

//     fn try_from(string: String) -> std::result::Result<Self, Self::Error> {
//         Self::try_from(string.as_str())
//     }
// }

// fn parse_fortune_file(path: &Path) -> Result<Fortunes> {
//     let string = read_to_string(path)
//         .wrap_err(format!("Failed to read fortune list from file: {}", path))?;
//     parse_fortunes(&string)
// }

pub fn hello_from_lib() {
    println!("Hello from lib.rs")
}
