use camino::Utf8PathBuf as PathBuf;

use clap::Parser;
use libfortune::hello_from_lib;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Path to the fortune file. The individual quotes should be seperated by "\n%\n"
    fortune_file: PathBuf,
}

fn main() {
    let fortunes = Args::parse().fortune_file;
    hello_from_lib();
}
